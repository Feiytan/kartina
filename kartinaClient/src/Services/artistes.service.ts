import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Artistes } from 'src/Models/artistes';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArtistesService {

  constructor(private httpClient:HttpClient) { }

  apiUrl:string = "https://localhost:44314/api/artistes";

  getArtistes():Observable<Artistes[]>{
    return this.httpClient.get<Artistes[]>(this.apiUrl)
  }

  getArtistesById(id:number){
    return this.httpClient.get<Artistes>(this.apiUrl + '/' + id)
  }
}
