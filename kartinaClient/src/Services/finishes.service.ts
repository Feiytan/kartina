import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Finish } from 'src/Models/finish';

@Injectable({
  providedIn: 'root'
})
export class FinishesService {
  private baseUrl:string;

  constructor(private httpClient:HttpClient) { 
    this.baseUrl = "https://localhost:44314/api/finishes";
  }

  public getFinishes():Observable<Finish[]>{
    return this.httpClient.get<Finish[]>(this.baseUrl);
  }

  public getFinishById(id:number):Observable<Finish>{
    return this.httpClient.get<Finish>(this.baseUrl+"/"+id);
  }
}
