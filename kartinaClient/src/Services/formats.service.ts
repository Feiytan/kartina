import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Format } from 'src/Models/format';

@Injectable({
  providedIn: 'root'
})
export class FormatsService {

  private baseUrl:string;

  constructor(private httpClient:HttpClient) {
    this.baseUrl = "https://localhost:44314/api/formats"
  }

  getFormats():Observable<Format[]>{
    return this.httpClient.get<Format[]>(this.baseUrl);
  }

  getFormatById(id:number):Observable<Format>{
    return this.httpClient.get<Format>(this.baseUrl+"/"+id);
  }
}
