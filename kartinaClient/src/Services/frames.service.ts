import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Frames } from '../Models/frames';

@Injectable({
  providedIn: 'root'
})
export class FramesService {

  private baseUrl:string = "https://localhost:44314/api/frames";
  constructor(private httpClient:HttpClient) { }
  
  public getFrames():Observable<Frames[]>{
    return this.httpClient.get<Frames[]>(this.baseUrl);
  }

  public getFrameById(id:number):Observable<Frames>{
    return this.httpClient.get<Frames>(this.baseUrl+"/"+id);
  }

}
