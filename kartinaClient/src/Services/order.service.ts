import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Order } from 'src/Models/order';
import { OrderLine } from 'src/Models/order-line';
import { HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private baseUrl:string = "https://localhost:44314/api/orders";

  constructor(private httpClient:HttpClient, private cookieService:CookieService) { }

  addOrder(order:Order){
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': "Bearer " + this.cookieService.get("token")
      })
    };
    return this.httpClient.post<Order>(this.baseUrl, order, httpOptions);
  }
}
