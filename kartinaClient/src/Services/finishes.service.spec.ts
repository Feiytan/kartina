import { TestBed } from '@angular/core/testing';

import { FinishesService } from './finishes.service';

describe('FinishesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FinishesService = TestBed.get(FinishesService);
    expect(service).toBeTruthy();
  });
});
