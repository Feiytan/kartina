export class CurrentUser {
    lastName:string;
    firstName:string;
    dateOfBirth:Date; 
    email:string;
    phoneNumber:string;
    profile:string;
    gender:string;
    token:string;
}
