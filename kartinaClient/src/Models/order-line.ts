import { Articles } from './articles';
import { Format } from './format';
import { Finish } from './finish';
import { FramesService } from 'src/Services/frames.service';
import { Frames } from './frames';

export class OrderLine {
    public id:number;
    public articleId:number;
    public article:Articles;
    public formatId:number;
    public format:Format;
    public finishId:number;
    public finish:Finish;
    public frameId:number;
    public frame:Frames;
    public orderId:number;
    public quantity:number;
    public price:number;
    public tva:number;

    /**
     *
     */
    constructor(quantity:number, price:number) {
        this.quantity = quantity;
        this.price = price;
        this.tva = 20;
    }
    
}
