export class Format {
    public id:number;
    public frName:string;
    public description:string;
    public percentagePriceChange:number;
    public imageUrl:string;
}

