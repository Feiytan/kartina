import { Articles } from '../Models/articles'

export class ArticlesReturn {
    articles:Articles[];
    numberOfArticles:number;
}
