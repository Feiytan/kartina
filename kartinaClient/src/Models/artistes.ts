export class Artistes {
    nickName:string;
    frPresentation:string;
    enPresentation:string;
    proEmail:string;
    twitter:string;
    facebook:string;
    printerest:string;
    webSite:string;
    tableName:string;
    avatarURL:string;
}