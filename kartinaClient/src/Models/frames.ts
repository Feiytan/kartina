export class Frames {
    public id:number;
    public enName:string;
    public frName:string;
    public imageUrl:string;
    public percentagePriceChange:number;
    public frDescription:string;
    public formatId:number;
}
