import { Component, OnInit } from '@angular/core';
import { ArticlesService } from 'src/Services/articles.service';
import { Articles } from '../../Models/articles';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private images: Articles[];
  private carousel: Articles[];

  constructor(private articlesService: ArticlesService) {
    this.images = new Array();
    this.carousel = new Array();
  }

  ngOnInit() {
    this.articlesService.getArticlesHome().subscribe(
      (images: Articles[]) => this.images = images,
      error => console.log(error.message),
    )
    this.articlesService.getArticlesCarousel().subscribe(
      (carousel: Articles[]) => this.carousel = carousel,
      error => console.log(error.message),
    )
  }
  
}
