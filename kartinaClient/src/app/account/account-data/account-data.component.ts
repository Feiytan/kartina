import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-account-data',
  templateUrl: './account-data.component.html',
  styleUrls: ['./account-data.component.scss']
})
export class AccountDataComponent implements OnInit {

  private firstName:string;
  private lastName:string;
  private dateOfBirth:string;
  private phoneNumber:string;
  private email:string;
  private profile:string;

  constructor(private cookie:CookieService) { }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo(){
    this.firstName = this.cookie.get("firstName"); 
    this.lastName = this.cookie.get("lastName"); 
    this.profile = this.cookie.get("profile"); 
    this.phoneNumber = this.cookie.get("phoneNumber"); 
    this.email = this.cookie.get("email"); 
    this.dateOfBirth = this.cookie.get("dateOfBirth"); 
  }

}
