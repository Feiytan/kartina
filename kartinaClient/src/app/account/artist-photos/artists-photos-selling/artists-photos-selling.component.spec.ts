import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistsPhotosSellingComponent } from './artists-photos-selling.component';

describe('ArtistsPhotosSellingComponent', () => {
  let component: ArtistsPhotosSellingComponent;
  let fixture: ComponentFixture<ArtistsPhotosSellingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistsPhotosSellingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistsPhotosSellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
