import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistsPhotosAddComponent } from './artists-photos-add.component';

describe('ArtistsPhotosAddComponent', () => {
  let component: ArtistsPhotosAddComponent;
  let fixture: ComponentFixture<ArtistsPhotosAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistsPhotosAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistsPhotosAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
