import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistPhotosComponent } from './artist-photos.component';

describe('ArtistPhotosComponent', () => {
  let component: ArtistPhotosComponent;
  let fixture: ComponentFixture<ArtistPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistPhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
