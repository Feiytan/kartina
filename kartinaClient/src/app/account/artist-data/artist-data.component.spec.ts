import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistDataComponent } from './artist-data.component';

describe('ArtistDataComponent', () => {
  let component: ArtistDataComponent;
  let fixture: ComponentFixture<ArtistDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
