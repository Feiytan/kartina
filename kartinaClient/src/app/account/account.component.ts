import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { LoginComponent } from '../auth/login/login.component';
import { LoginService } from 'src/Services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  private userName:string;
  private userProfile:string;
  constructor(private cookies:CookieService, private loginService:LoginService, private router:Router) { }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo(){
    this.userName = this.cookies.get("firstName"); 
    this.userProfile = this.cookies.get("profile"); 
  }

  logout(){
    console.log("logout")
    this.cookies.deleteAll();
    this.loginService.userChanged();
    this.router.navigate(['/'])
  }

}
