import { Component } from '@angular/core';
import { Ng7BootstrapBreadcrumbService } from 'ng7-bootstrap-breadcrumb';
import $ from 'jquery';
import { slideInAnimation } from './route-animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ slideInAnimation ]
})
export class AppComponent {
  title = 'Kartina';
  articles = new Array();

  constructor(private ng7BootstrapBreadcrumbService: Ng7BootstrapBreadcrumbService) {
  }

  onActivate(event) {
    window.scroll(0, 0);
  }
  
  ngOnInit() {
    /* Cache la mainNav quand on clique en dehors */
    $(document).ready(function() {
      $(document).click(function(event) {
        var clickover = $(event.target);
        var opened = $(".navbar-collapse").hasClass("show");
        if (opened === true && !clickover.hasClass("navbar-toggler" ) && !clickover.hasClass("form-control" )) {
          $(".navbar-toggler").click();
        }
      });
    });
  }
}
