import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {Ng7BootstrapBreadcrumbModule} from "ng7-bootstrap-breadcrumb";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HelpComponent } from './help/help.component';
import { CartComponent } from './cart/cart.component';
import { PhotosComponent } from './photos/photos.component';
import { ConditionsComponent } from './conditions/conditions.component';
import { LegalMentionsComponent } from './legal-mentions/legal-mentions.component';
import { AccountComponent } from './account/account.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthComponent } from './auth/auth.component';
import { BuyingPathComponent } from './buying-path/buying-path.component';
import { FormatComponent } from './buying-path/format/format.component';
import { FinishComponent } from './buying-path/finish/finish.component';
import { FrameComponent } from './buying-path/frame/frame.component';
import { ArtistsComponent } from './artists/artists.component';
import { ArtistDetailComponent } from './artist-detail/artist-detail.component';
import { OrdersComponent } from './account/orders/orders.component';
import { AddressComponent } from './account/address/address.component';
import { AccountDataComponent } from './account/account-data/account-data.component';
import { AdminOrdersComponent } from './account/admin-orders/admin-orders.component';
import { AdminSellersComponent } from './account/admin-sellers/admin-sellers.component';
import { ArtistPhotosComponent } from './account/artist-photos/artist-photos.component';
import { ArtistDataComponent } from './account/artist-data/artist-data.component';
import { ArtistsPhotosAddComponent } from './account/artist-photos/artists-photos-add/artists-photos-add.component';
import { ArtistsPhotosSellingComponent } from './account/artist-photos/artists-photos-selling/artists-photos-selling.component';
import { ArtistsPhotosSoldComponent } from './account/artist-photos/artists-photos-sold/artists-photos-sold.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavMenuComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    HelpComponent,
    CartComponent,
    PhotosComponent,
    ConditionsComponent,
    LegalMentionsComponent,
    AccountComponent,
    PageNotFoundComponent,
    AuthComponent,
    BuyingPathComponent,
    FormatComponent,
    FinishComponent,
    FrameComponent,
    ArtistsComponent,
    ArtistDetailComponent,
    OrdersComponent,
    AddressComponent,
    AccountDataComponent,
    AdminOrdersComponent,
    AdminSellersComponent,
    ArtistPhotosComponent,
    ArtistDataComponent,
    ArtistsPhotosAddComponent,
    ArtistsPhotosSellingComponent,
    ArtistsPhotosSoldComponent
  ],
  imports: [
    BrowserAnimationsModule, 
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    Ng7BootstrapBreadcrumbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
