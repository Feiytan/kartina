import { Component, OnInit } from '@angular/core';
import { FramesService } from 'src/Services/frames.service';
import { Frames } from 'src/Models/frames'
import { CookieService } from 'ngx-cookie-service';
import { OrderLine } from 'src/Models/order-line';
import { Order } from 'src/Models/order';
import { Articles } from 'src/Models/articles';
import { Format } from 'src/Models/format';
import { Finish } from 'src/Models/finish';
import { ArticlesService } from 'src/Services/articles.service';
import { FinishesService } from 'src/Services/finishes.service';
import { FormatsService } from 'src/Services/formats.service';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { CartService } from 'src/Services/cart.service';

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styleUrls: ['./frame.component.scss']
})
export class FrameComponent implements OnInit {

  private frames:Frames[];
  private selection:number = 0;
  private itemPrice:number;
  private oriderLine:OrderLine;

  private curentFinish:number;
  private curentFormat:number;

  constructor(private framesService:FramesService, private articleService:ArticlesService, private finishesService:FinishesService, private formatService:FormatsService,private _location: Location, private router:Router, private cartService:CartService) {
  }

  ngOnInit() {
    this.curentFinish = parseInt(sessionStorage.getItem("finish"));
    this.curentFormat = parseInt(sessionStorage.getItem("format"));

    this.framesService.getFrames().subscribe(
      frames => this.frames = frames,
      error => console.log(error.message)
    )
    this.itemPrice = parseFloat(sessionStorage.getItem("priceWithFinish"));
  }

  backClicked() {
    this._location.back();
  }

  setSelection(id:number){
    this.selection = id;
  }

  checkIfFinish(orderLine:OrderLine){
    if (orderLine.article != null && orderLine.format != null && orderLine.frame != null && orderLine.finish != null){
      let order : OrderLine[];
      order = localStorage.getItem("cart") === null ? new Array() : JSON.parse(localStorage.getItem("cart"))
      if(order.length > 0)
      {
        for (let i = 0; i < order.length; i++) {
          if (order[i].article.id === orderLine.article.id && order[i].format.id === orderLine.format.id && order[i].frame.id === orderLine.frame.id && order[i].finish.id === orderLine.finish.id) 
          {
            order[i].quantity++;
            break;
          }
          else if (i+1 === order.length)
          {
            order.push(orderLine);
            break;
          }
        }
      }
      else
        order.push(orderLine);

      localStorage.setItem("cart", JSON.stringify(order));
      this.cartService.cartChanged();
      sessionStorage.clear();
      this.router.navigate(['/cart'])
    }
  }

  saveChoice():void{
    let totalPrice:number;
    
    let articleId:number = parseInt(sessionStorage.getItem("article"));
    let formatId:number = parseInt(sessionStorage.getItem("format"));
    let finishId:number = parseInt(sessionStorage.getItem("finish"));
    let quantity:number = 1;
    let order:OrderLine[];
    
    if (this.selection > 0)
    sessionStorage.setItem("frame", this.selection.toString());
    
    this.frames.forEach(frame => {
      if(frame.id === this.selection)
      totalPrice = this.itemPrice * (frame.percentagePriceChange / 100)
    });
    
    let orderLine:OrderLine = new OrderLine(1, totalPrice);

    this.articleService.getArticlesById(articleId).subscribe(
      item =>{
        orderLine.article = item;
        orderLine.articleId = orderLine.article.id;
      },
      error => {
        orderLine.article = new Articles();
        console.log(error.message);
      },
      () => this.checkIfFinish(orderLine)
    );

    this.framesService.getFrameById(this.selection).subscribe(
      item => {
        orderLine.frame = item,
        orderLine.frameId = item.id;
      },
      () =>{
        orderLine.frame = new Frames();
        orderLine.frameId = this.selection;
      },
      () => this.checkIfFinish(orderLine)
    );

    this.finishesService.getFinishById(finishId).subscribe(
      item => {
        orderLine.finish = item;
        orderLine.finishId = orderLine.finish.id;
      },
      () => orderLine.finish = new Finish(),
      () => this.checkIfFinish(orderLine)
    )

    this.formatService.getFormatById(formatId).subscribe(
      item => {
        orderLine.format = item;
        orderLine.formatId = orderLine.format.id;
      },
      error => orderLine.format = new Format(),
      () => this.checkIfFinish(orderLine)
    );
  }

}
