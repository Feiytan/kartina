import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/Services/cart.service';
import { Order } from 'src/Models/order';
import { OrderLine } from 'src/Models/order-line';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from 'src/Services/login.service';


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit {

  private numberOfItemsInCart:number;
  private userName:string;
  private cart:OrderLine[];

  constructor(cartService : CartService, private cookie:CookieService, loginService:LoginService) {
    this.getNumberOfItemsInCart();
    cartService.getCartChanged().subscribe(
      () => this.getNumberOfItemsInCart()
    );
    this.getUserName();
    loginService.getUserChanged().subscribe(
      () => this.getUserName()
    );
    console.log(this.userName);
   }

  getUserName(){
    this.userName = this.cookie.get("firstName"); 
  }

  getNumberOfItemsInCart(){
    let cart:OrderLine[] = JSON.parse(localStorage.getItem("cart"));
    this.numberOfItemsInCart = cart.length;
    console.log(this.numberOfItemsInCart);
  }

  ngOnInit() {
  }

}
