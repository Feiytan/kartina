import { Component, OnInit } from '@angular/core';
import { Artistes } from 'src/Models/artistes';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ArtistesService } from 'src/Services/artistes.service';
import { Articles } from 'src/Models/articles';
import { ArticlesService } from 'src/Services/articles.service';
import { cardAnimation } from '../route-animation';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.scss'],
  animations: [cardAnimation]
})
export class ArtistDetailComponent implements OnInit {
  private id: number;
  private currentArtiste: Artistes;
  private articles: Articles[];

  constructor(private router: Router, private route: ActivatedRoute, private articlesService: ArticlesService, private artisteService: ArtistesService) {
    this.id = this.route.snapshot.params.id;
    this.articles = new Array();
    this.currentArtiste = new Artistes();
  }

  ngOnInit() {
    this.getArtiste();
    this.getArticles();
  }

  getArtiste() {
    this.artisteService.getArtistesById(this.id).subscribe(
      currentArtiste => { this.currentArtiste = currentArtiste },
      error => console.log(error.message)
    )
  }

  getArticles() {
    this.articlesService.getArticlesByArtiste(this.id).subscribe(
      (articles: Articles[]) => this.articles = articles,
      error => console.log(error.message),
    )
  }

}
