import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validator, Validators } from '@angular/forms';
import { LoginService } from 'src/Services/login.service';
import { AuthentificationInfos } from 'src/Models/authentification-infos';
import { error } from 'util';
import { CookieService } from 'ngx-cookie-service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  message:string;
  loginForm:FormGroup;
  constructor(private fb:FormBuilder, private loginService:LoginService, private CookieService:CookieService, private router:Router)
  {  
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.minLength(4), Validators.required]]
    });
  }

  onSubmit(){
    this.message = "Connexion en cours..."
    this.loginService.login(new AuthentificationInfos(this.loginForm.controls["email"].value, this.loginForm.controls["password"].value))
      .subscribe(currentUser => {
        this.CookieService.set("firstName", currentUser.firstName);
        this.CookieService.set("lastName", currentUser.lastName);
        this.CookieService.set("dateOfBirth", currentUser.dateOfBirth.toString());
        this.CookieService.set("email", currentUser.email);
        this.CookieService.set("phoneNumber", currentUser.phoneNumber);
        this.CookieService.set("gender", currentUser.gender);
        this.CookieService.set("profile", currentUser.profile);
        this.CookieService.set("token", currentUser.token);
        this.loginService.userChanged();
      },err => {this.message = err.status == 400 ?
        "Veuillez entrer un mail et un mot de passe valide" :
        "Veuillez vérifier votre connexion internet, si le problème persiste, contactez un administrateur depuis le menu aide"},
      () => this.router.navigate(['']));

  }
}
