﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class AllowingnullonThemeIdandArtisteId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Artists_ArtisteId",
                table: "Articles");

            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles");

            migrationBuilder.AlterColumn<int>(
                name: "ThemeId",
                table: "Articles",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ArtisteId",
                table: "Articles",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Artists_ArtisteId",
                table: "Articles",
                column: "ArtisteId",
                principalTable: "Artists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Artists_ArtisteId",
                table: "Articles");

            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles");

            migrationBuilder.AlterColumn<int>(
                name: "ThemeId",
                table: "Articles",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ArtisteId",
                table: "Articles",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Artists_ArtisteId",
                table: "Articles",
                column: "ArtisteId",
                principalTable: "Artists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
