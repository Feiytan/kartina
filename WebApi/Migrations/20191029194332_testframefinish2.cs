﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class testframefinish2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinishId",
                table: "Frames");

            migrationBuilder.AddColumn<int>(
                name: "FormatId",
                table: "Frames",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FormatId",
                table: "Frames");

            migrationBuilder.AddColumn<int>(
                name: "FinishId",
                table: "Frames",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
