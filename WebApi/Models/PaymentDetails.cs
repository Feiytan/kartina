﻿namespace WebApi.Models
{
    public class PaymentDetails
    {
        public int Id { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string AccountHolder { get; set; }
        public User User {get; set; }
    }
}
