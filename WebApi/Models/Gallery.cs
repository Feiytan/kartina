﻿using System.Collections.Generic;
namespace WebApi.Models
{
    public class Gallery
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AddressCounty{ get; set; }
        public string AddressStreet { get; set; }
        public string AddressNumber { get; set; }
        public string AddressPostalCode { get; set; }
        public string AddressCity { get; set; }
        public string Mail { get; set; }
        public string PhoneNumber { get; set; }
        public User User { get; set; }
        public List<Article> Articles {get; set;}
    }
}
