﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    public class Address
    {
        public int Id{ get; set; }
        [Column(TypeName = "varchar(30)")]
        public string Street { get; set; }
        public string Number { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string PostalCode { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string City { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string Country { get; set; }
        public User User {get; set;}
    }
}
