using Microsoft.EntityFrameworkCore;
namespace WebApi.Models
{
    public class KartinaContext : DbContext
    {
         public KartinaContext(DbContextOptions<KartinaContext> options)
            : base(options)
        {          
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderedArticle>()
                .HasKey(o => new { o.ArticleId, o.FinishId, o.FormatId, o.FrameId, o.OrderId });
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Artiste> Artists { get; set; }
        public DbSet<Finish> Finishes { get; set; }
        public DbSet<Format> Formats { get; set; }
        public DbSet<Frame> Frames { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<LegalInformation> LegalInformations { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderedArticle> OrderedArticles { get; set; }
        public DbSet<PaymentDetails> PaymentDetails { get; set; }
        public DbSet<Theme> Themes { get; set; }
        public DbSet<User> Users { get; set; }

    }

}