using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FramesController : ControllerBase
    {
        private readonly KartinaContext _context;

        public FramesController(KartinaContext context)
        {
            _context = context;
        }

        // GET: api/Frames
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Frame>>> GetFrames()
        {
            return await _context.Frames.OrderBy(frame => frame.PercentagePriceChange).ToListAsync();
        }

        // GET: api/Frames/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Frame>> GetFrame(int id)
        {
            var frame = await _context.Frames.FindAsync(id);

            if (frame == null)
            {
                return NotFound();
            }

            return frame;
        }

        // PUT: api/Frames/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFrame(int id, Frame frame)
        {
            if (id != frame.Id)
            {
                return BadRequest();
            }

            _context.Entry(frame).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FrameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Frames
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Frame>> PostFrame(Frame frame)
        {
            _context.Frames.Add(frame);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFrame", new { id = frame.Id }, frame);
        }

        // DELETE: api/Frames/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Frame>> DeleteFrame(int id)
        {
            var frame = await _context.Frames.FindAsync(id);
            if (frame == null)
            {
                return NotFound();
            }

            _context.Frames.Remove(frame);
            await _context.SaveChangesAsync();

            return frame;
        }

        private bool FrameExists(int id)
        {
            return _context.Frames.Any(e => e.Id == id);
        }
    }
}
