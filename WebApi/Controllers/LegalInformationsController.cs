using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LegalInformationsController : ControllerBase
    {
        private readonly KartinaContext _context;

        public LegalInformationsController(KartinaContext context)
        {
            _context = context;
        }

        // GET: api/LegalInformations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LegalInformation>>> GetLegalInformations()
        {
            return await _context.LegalInformations.ToListAsync();
        }

        // GET: api/LegalInformations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LegalInformation>> GetLegalInformation(int id)
        {
            var legalInformation = await _context.LegalInformations.FindAsync(id);

            if (legalInformation == null)
            {
                return NotFound();
            }

            return legalInformation;
        }

        // PUT: api/LegalInformations/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLegalInformation(int id, LegalInformation legalInformation)
        {
            if (id != legalInformation.Id)
            {
                return BadRequest();
            }

            _context.Entry(legalInformation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LegalInformationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LegalInformations
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<LegalInformation>> PostLegalInformation(LegalInformation legalInformation)
        {
            _context.LegalInformations.Add(legalInformation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLegalInformation", new { id = legalInformation.Id }, legalInformation);
        }

        // DELETE: api/LegalInformations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<LegalInformation>> DeleteLegalInformation(int id)
        {
            var legalInformation = await _context.LegalInformations.FindAsync(id);
            if (legalInformation == null)
            {
                return NotFound();
            }

            _context.LegalInformations.Remove(legalInformation);
            await _context.SaveChangesAsync();

            return legalInformation;
        }

        private bool LegalInformationExists(int id)
        {
            return _context.LegalInformations.Any(e => e.Id == id);
        }
    }
}
