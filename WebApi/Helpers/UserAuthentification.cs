﻿using System;
using WebApi.Models;
namespace WebApi.Helpers
{
    public class UserAuthentification
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Profile { get; set; }
        public string Gender { get; set; }
        public string Token { get; set; }

        public UserAuthentification(User user)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            DateOfBirth = user.DateOfBirth;
            Email = user.Email;
            PhoneNumber = user.PhoneNumber;
            Profile = user.Profile;
            Gender = user.Gender;
        }
    }
}
