/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Addresses]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addresses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Street] [varchar](30) NULL,
	[Number] [nvarchar](max) NULL,
	[PostalCode] [varchar](30) NULL,
	[City] [varchar](30) NULL,
	[Country] [varchar](30) NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Articles]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NULL,
	[Stock] [int] NOT NULL,
	[ImageUrl] [varchar](100) NULL,
	[Tags] [nvarchar](max) NULL,
	[AdditionDate] [datetime2](7) NOT NULL,
	[Orientation] [varchar](30) NULL,
	[SpecialOffer] [float] NOT NULL,
	[FrDescription] [nvarchar](max) NULL,
	[EnDescription] [nvarchar](max) NULL,
	[Price] [float] NOT NULL,
	[ThemeId] [int] NULL,
	[ArtisteId] [int] NULL,
	[GalleryId] [int] NULL,
	[NbSales] [int] NOT NULL,
 CONSTRAINT [PK_Articles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Artists]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Artists](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NickName] [nvarchar](max) NULL,
	[FrPresentation] [nvarchar](max) NULL,
	[EnPresentation] [nvarchar](max) NULL,
	[ProEmail] [nvarchar](max) NULL,
	[Twitter] [nvarchar](max) NULL,
	[Facebook] [nvarchar](max) NULL,
	[Printerest] [nvarchar](max) NULL,
	[WebSite] [nvarchar](max) NULL,
	[TableName] [nvarchar](max) NULL,
	[UserId] [int] NULL,
	[AvatarURL] [nvarchar](max) NULL,
	[Instagram] [nvarchar](max) NULL,
 CONSTRAINT [PK_Artists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Finishes]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Finishes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FrName] [nvarchar](max) NULL,
	[EnName] [nvarchar](max) NULL,
	[FrDescription] [nvarchar](max) NULL,
	[EnDescription] [nvarchar](max) NULL,
	[PercentagePriceChange] [float] NOT NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[FormatId] [int] NOT NULL,
 CONSTRAINT [PK_Finishes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Formats]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Formats](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FrName] [nvarchar](max) NULL,
	[EnName] [nvarchar](max) NULL,
	[Size] [nvarchar](max) NULL,
	[PercentagePriceChange] [float] NOT NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Formats] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Frames]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Frames](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FrName] [nvarchar](max) NULL,
	[EnName] [nvarchar](max) NULL,
	[FrDescription] [nvarchar](max) NULL,
	[PercentagePriceChange] [float] NOT NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[FormatId] [int] NOT NULL,
 CONSTRAINT [PK_Frames] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Galleries]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Galleries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[AddressCounty] [nvarchar](max) NULL,
	[AddressStreet] [nvarchar](max) NULL,
	[AddressNumber] [nvarchar](max) NULL,
	[AddressPostalCode] [nvarchar](max) NULL,
	[AddressCity] [nvarchar](max) NULL,
	[Mail] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_Galleries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Icon] [nvarchar](max) NULL,
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LegalInformations]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LegalInformations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[FrTitle] [nvarchar](max) NULL,
	[EnTitle] [nvarchar](max) NULL,
	[FrText] [nvarchar](max) NULL,
	[EnText] [nvarchar](max) NULL,
 CONSTRAINT [PK_LegalInformations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FrTitle] [nvarchar](max) NULL,
	[EnTitle] [nvarchar](max) NULL,
	[FrText] [nvarchar](max) NULL,
	[EnText] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
	[Date] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderedArticles]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderedArticles](
	[ArticleId] [int] NOT NULL,
	[FormatId] [int] NOT NULL,
	[FinishId] [int] NOT NULL,
	[FrameId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[Tva] [float] NOT NULL,
 CONSTRAINT [PK_OrderedArticles] PRIMARY KEY CLUSTERED 
(
	[ArticleId] ASC,
	[FinishId] ASC,
	[FormatId] ASC,
	[FrameId] ASC,
	[OrderId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderDate] [datetime2](7) NOT NULL,
	[Delivery] [nvarchar](max) NULL,
	[DeliveryCosts] [float] NOT NULL,
	[AddresStreet] [nvarchar](max) NULL,
	[AddressNumber] [nvarchar](max) NULL,
	[AddressPostalCode] [nvarchar](max) NULL,
	[AddressCity] [nvarchar](max) NULL,
	[AdressCountry] [nvarchar](max) NULL,
	[GalleryId] [int] NULL,
	[UserId] [int] NOT NULL,
	[Status] [nvarchar](max) NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaymentDetails]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CardType] [nvarchar](max) NULL,
	[CardNumber] [nvarchar](max) NULL,
	[AccountHolder] [nvarchar](max) NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_PaymentDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Themes]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Themes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Themes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 30/10/2019 10:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](255) NULL,
	[Gender] [varchar](30) NULL,
	[FirstName] [varchar](30) NULL,
	[LastName] [varchar](50) NULL,
	[DateOfBirth] [datetime2](7) NOT NULL,
	[Password] [varchar](255) NULL,
	[Profile] [varchar](30) NULL,
	[PhoneNumber] [varchar](15) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191021132641_Initial migration', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191022093037_Change Mail to Email in Users', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191023085244_Ajout de ThemeId and ArtisteId sur la table products', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191023092631_Allowing null on ThemeId and ArtisteId', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191024095241_Ajout ImageUrl', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191024131804_Ajout de status sur la table Orders', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191025105026_Ajout nombre de ventes sur la table articles', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191026104614_Ajout de imageUrl dans le modele Frame', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191026134256_Ajout de ImageUrl dans la table Format', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191026200050_Ajout de image url dans la table finish', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191027205755_artiste', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191027211509_artisteinsta', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191028091327_descriptionFormat', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191028152045_descriptionTheme', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191029193036_testframefinish', N'3.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191029194332_testframefinish2', N'3.0.0')
SET IDENTITY_INSERT [dbo].[Addresses] ON 

INSERT [dbo].[Addresses] ([Id], [Street], [Number], [PostalCode], [City], [Country], [UserId]) VALUES (1, N'Avenue Jean Jaures', N'15', N'67000', N'Strasbourg', N'France', 1)
INSERT [dbo].[Addresses] ([Id], [Street], [Number], [PostalCode], [City], [Country], [UserId]) VALUES (4, N'Rue Robert Schuman', N'237', N'67100', N'Strasbourg', N'France', 23)
INSERT [dbo].[Addresses] ([Id], [Street], [Number], [PostalCode], [City], [Country], [UserId]) VALUES (5, N'Rue Henri Matis', N'4', N'68200', N'Mulhouse', N'France', 24)
INSERT [dbo].[Addresses] ([Id], [Street], [Number], [PostalCode], [City], [Country], [UserId]) VALUES (6, N'Avenue Champ Elysée', N'1', N'75000', N'Paris', N'France', 25)
INSERT [dbo].[Addresses] ([Id], [Street], [Number], [PostalCode], [City], [Country], [UserId]) VALUES (7, N'Via Pietro Cossa', N'5', N'00193', N'Roma', N'Italie', 29)
SET IDENTITY_INSERT [dbo].[Addresses] OFF
SET IDENTITY_INSERT [dbo].[Articles] ON 

INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (6, N'Derniers exemplaires', 10, N'/assets/home-img/Derniers_exemplaires.jpg', N'exemplaires', CAST(N'2004-12-12T00:00:00.0000000' AS DateTime2), N'Panoramique', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 250, 2, 1, NULL, 2342)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (25, N'Edifice Oublié', 2, N'/assets/home-img/edifice_oublier.jpg', N'edifice oublier', CAST(N'2002-10-12T00:00:00.0000000' AS DateTime2), N'Panoramique', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 199, 2, 4, NULL, 324)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (26, N'Rues', 5, N'/assets/home-img/Aurélien_Vilette01.jpg', N'Aurélien Vilette', CAST(N'2005-12-12T00:00:00.0000000' AS DateTime2), N'Panoramique', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 129, 2, 2, NULL, 234)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (36, N'Paris Noir & Blanc', 27, N'/assets/home-img/noir_et_blanc_paris.jpg', N'noir et blanc paris', CAST(N'2013-02-02T00:00:00.0000000' AS DateTime2), N'Panoramique', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 99, 3, 3, NULL, 324)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (42, N'City', 23, N'/assets/home-img/slide1.jpg', N'slide1', CAST(N'2019-12-08T00:00:00.0000000' AS DateTime2), N'Panoramique', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 89, 3, 4, NULL, 23)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (47, N'Photographe', 9, N'/assets/home-img/slide2.jpg', N'slide2', CAST(N'2019-12-01T00:00:00.0000000' AS DateTime2), N'Panoramique', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 119, 1, 3, NULL, 5)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (48, N'Galerie Noir Blanc', 4, N'/assets/home-img/slide3.jpg', N'slide3', CAST(N'2017-08-06T00:00:00.0000000' AS DateTime2), N'Panoramique', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 79, 3, 1, NULL, 324)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (49, N'Voyages', 12, N'/assets/home-img/Voyages.jpg', N'Voyages', CAST(N'2000-09-07T00:00:00.0000000' AS DateTime2), N'Panoramique', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 69, 2, 5, NULL, 435)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (54, N'Alice in Wonderland', 3, N'https://www.fairyday.net/sy-photos/page-photos/20180221104718-Alice_Sleeping.jpg', N'Alice sleep Edition limitée', CAST(N'2011-10-24T00:00:00.0000000' AS DateTime2), N'Portrait', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 149, 2, 5, NULL, 56)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (55, N'Red Apple', 29, N'https://www.pictorem.com/collection/900_768279.jpg', N'Red apple Edition limitée', CAST(N'2013-02-09T00:00:00.0000000' AS DateTime2), N'Paysage', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 201, 1, 1, NULL, 546)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (57, N'Edifices Oubliés 2', 2, N'https://images.saatchiart.com/saatchi/746948/art/3071762/2141655-RECXBGDF-7.jpg', N'Edifices oubliés Edition limitée', CAST(N'2019-04-20T00:00:00.0000000' AS DateTime2), N'Paysage', 5, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 198, 3, 3, NULL, 546)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (58, N'Paris', 13, N'https://www.thewanderinglens.com/wp-content/uploads/2018/01/header.jpg', N'Paris Edition limitée', CAST(N'2009-09-20T00:00:00.0000000' AS DateTime2), N'Paysage', 7, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 99, 2, 4, NULL, 456)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (59, N'Space', 9, N'https://live.staticflickr.com/8325/29759898216_cc8106a8cd_b.jpg', N'Space Edition limitée', CAST(N'2008-03-30T00:00:00.0000000' AS DateTime2), N'Carré', 11, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 152, 2, 2, NULL, 5)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (60, N'History', 0, N'https://blogs.icrc.org/law-and-policy/wp-content/uploads/sites/102/2017/02/war-photographer.png', N'History Edition limitée', CAST(N'1999-10-13T00:00:00.0000000' AS DateTime2), N'Paysage', 30, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 95, 3, 1, NULL, 546)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (61, N'Portrait', 10, N'https://www.artmajeur.com/medias/standard/y/l/ylart/artwork/11112343_pict0739.jpg', N'Portrait', CAST(N'2004-10-15T00:00:00.0000000' AS DateTime2), N'Portrait', 20, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 45, 2, 5, NULL, 45)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (62, N'Portrait Nature', 20, N'http://japanization.org/wp-content/uploads/2016/07/04_Miho_Hirano-696x696.jpg', N'Portrait', CAST(N'2002-10-08T00:00:00.0000000' AS DateTime2), N'Carré', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 85, 1, 5, NULL, 456)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (63, N'Iles', 10, N'https://www.bouger-voyager.com/wp-content/uploads/2018/02/quelles-iles-visiter-%C3%A0-tahiti.jpg', N'Paysage', CAST(N'2018-05-05T00:00:00.0000000' AS DateTime2), N'Paysage', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 20, 1, 4, NULL, 456)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (64, N'Monk Beach', 10, N'http://www.partagedevoyages.com/wp-content/uploads/sites/8/2018/05/Voyage-paysage.jpg', N'Paysage', CAST(N'2012-02-05T00:00:00.0000000' AS DateTime2), N'Paysage', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 20, 1, 4, NULL, 4567)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (65, N'Chill', 20, N'https://i.ytimg.com/vi/eHZVdHop680/maxresdefault.jpg', N'Paysage', CAST(N'2013-02-05T00:00:00.0000000' AS DateTime2), N'Paysage', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 120, 1, 3, NULL, 0)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (66, N'Henry Potier', 45, N'http://idata.over-blog.com/1/23/06/43//HP.jpg', N'Portrait', CAST(N'1999-04-05T00:00:00.0000000' AS DateTime2), N'Portrait', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 210, 2, 5, NULL, 8756)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (67, N'Ireland Cliffs', 456, N'https://regismatthey.files.wordpress.com/2012/08/2012-08-18_cliffs-of-mohrer_072.jpg?w=633', N'Portrait', CAST(N'2015-08-05T00:00:00.0000000' AS DateTime2), N'Portrait', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 300, 1, 2, NULL, 3456)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (68, N'Des Ballons', 45, N'https://i.pinimg.com/originals/54/e0/c5/54e0c53ae443ffd52ccb6b1382142072.jpg', N'Portrait', CAST(N'2012-05-05T00:00:00.0000000' AS DateTime2), N'Portrait', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 138, 3, 1, NULL, 98)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (73, N'Falaise', 456, N'https://microsites.lomography.com/lc-wide-fr/content/galleries/square-format/square-format.08.jpg', N'Carré', CAST(N'2015-05-02T00:00:00.0000000' AS DateTime2), N'Carré', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 148, 1, 2, NULL, 3456)
INSERT [dbo].[Articles] ([Id], [Name], [Stock], [ImageUrl], [Tags], [AdditionDate], [Orientation], [SpecialOffer], [FrDescription], [EnDescription], [Price], [ThemeId], [ArtisteId], [GalleryId], [NbSales]) VALUES (74, N'Grande Roue', 56, N'https://www.dpreview.com/files/p/articles/9462076757/1723747.jpeg', N'Carré', CAST(N'2014-05-02T00:00:00.0000000' AS DateTime2), N'Carré', 0, N'Cette œuvre hétérogène témoigne de la vitalité d’un touche-à-tout spontanément artiste qui intègre tout ce qu’il aime à son art : non seulement ses compagnes, ses enfants, mais aussi le cirque, la tauromachie, l’Espagne, la politique… jusqu’aux objets de pacotille soigneusement conservés que l’on retrouve assemblés dans ses sculptures.', N'This heterogeneous work testifies to the vitality of a jack-of-all-trades artist who integrates everything he likes to his art: not only his companions, his children, but also the circus, bullfighting, Spain, politics ... to carefully preserved junk objects found in his sculptures.', 523, 2, 3, NULL, 23)
SET IDENTITY_INSERT [dbo].[Articles] OFF
SET IDENTITY_INSERT [dbo].[Artists] ON 

INSERT [dbo].[Artists] ([Id], [NickName], [FrPresentation], [EnPresentation], [ProEmail], [Twitter], [Facebook], [Printerest], [WebSite], [TableName], [UserId], [AvatarURL], [Instagram]) VALUES (1, N'PICASSO', N'Pablo Ruiz Picasso, né à Malaga (Espagne) le 25 octobre 1881 et mort le 8 avril 1973 à Mougins (Alpes-Maritimes, France), est un peintre, dessinateur, sculpteur et graveur espagnol ayant passé l''essentiel de sa vie en France.', N'
Pablo Ruiz Picasso, born in Malaga (Spain) on October 25, 1881 ,is a Spanish painter, draftsman, sculptor and engraver who spent most of his life in France .', N'picasso@technologyandstrategy.com', N'http://twitter.com', N'http://facebook.com', N'', N'', NULL, 29, N'https://www.expertisez.com/images/easyblog_images/512/s-PABLO-PICASSO-large640.jpg', N'http://instagram.com')
INSERT [dbo].[Artists] ([Id], [NickName], [FrPresentation], [EnPresentation], [ProEmail], [Twitter], [Facebook], [Printerest], [WebSite], [TableName], [UserId], [AvatarURL], [Instagram]) VALUES (2, N'MICELIO', N'Micelio, né à Strasbourg (France) le 12 Séptembre 1996 , st un peintre, dessinateur, sculpteur et graveur espagnol ayant passé l''essentiel de sa vie en France.', N'Micelio, born in Strasbourg (France) 12 Séptember 1996, is a Spanish painter, draftsman, sculptor and engraver who spent most of his life in France .', N'micelio@technologyandstrategy.com', N'', N'http://facebook.com', N'http://pinterest.com', N'http://micelio.com', NULL, 23, N'https://d3c0aoh0dus5lw.cloudfront.net/WP/wp-content/uploads/2017/11/cjasonbradley_170902_26266-864x577.jpg', NULL)
INSERT [dbo].[Artists] ([Id], [NickName], [FrPresentation], [EnPresentation], [ProEmail], [Twitter], [Facebook], [Printerest], [WebSite], [TableName], [UserId], [AvatarURL], [Instagram]) VALUES (3, N'BAUMANNO', N'Baumanno, né à Strasbourg (France) le 12 Décembre 1996 , st un peintre, dessinateur, sculpteur et graveur espagnol ayant passé l''essentiel de sa vie en France.', N'Baumanno, born in Strasbourg (France) le 12 Décembre 1996, is a Spanish painter, draftsman, sculptor and engraver who spent most of his life in France .', N'baumanno@technologyandstrategy.com', N'http://twitter.com', N'', N'', N'', NULL, 1, N'https://i.ytimg.com/vi/BEm8MzrdkJ0/maxresdefault.jpg', N'http://instagram.com')
INSERT [dbo].[Artists] ([Id], [NickName], [FrPresentation], [EnPresentation], [ProEmail], [Twitter], [Facebook], [Printerest], [WebSite], [TableName], [UserId], [AvatarURL], [Instagram]) VALUES (4, N'MECHEHOUDO', N'Mechehoudo, né à Oran (Algérie) le 01 Février 1984 , st un peintre, dessinateur, sculpteur et graveur espagnol ayant passé l''essentiel de sa vie en France.', N'Mechehoudo, born in Oran (Algérie) le 01 Février 1984, is a Spanish painter, draftsman, sculptor and engraver who spent most of his life in France .', N'mechehoudo@technologyandstrategy.com', N'http://twitter.com', N'http://facebook.com', N'', N'http://mechehoudo.com', NULL, 24, N'https://cdn.fstoppers.com/styles/large-16-9/s3/lead/2019/07/78fad4386ee2f3fd85352067d058f8a8.jpg', NULL)
INSERT [dbo].[Artists] ([Id], [NickName], [FrPresentation], [EnPresentation], [ProEmail], [Twitter], [Facebook], [Printerest], [WebSite], [TableName], [UserId], [AvatarURL], [Instagram]) VALUES (5, N'AVODAGBEIO', N'Avodagbeio, né à Strasbourg (France) le 11 Décembre 1986 , st un peintre, dessinateur, sculpteur et graveur espagnol ayant passé l''essentiel de sa vie en France.', N'Avodagbei, born in Strasbourg (France) le 11 Décembre 1986, is a Spanish painter, draftsman, sculptor and engraver who spent most of his life in France .', N'avodagbei@ajcformation.com', N'http://twitter.com', N'', N'http://pinterest.com', N'http://avodagbeio.com', NULL, 25, N'https://static.timesofisrael.com/jewishndev/uploads/2018/10/Max.jpg', N'http://instagram.com')
SET IDENTITY_INSERT [dbo].[Artists] OFF
SET IDENTITY_INSERT [dbo].[Finishes] ON 

INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (2, N'Blackout', N'Blackout', N'Tirage passe-partout noir avec liseret', N'correspond au prix de la photographie suite au choix du « Format ».Il n’y a pas de surcout induit par le choix de cette finition. Ainsi, un utilisateur ayant choisi le format « Classique » et la finition « Passe-partout noir » dispose du prix le plus bas.', 100, N'/assets/img/finish-4.PNG', 5)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (3, N'Artshot', N'Artshot', N'Tirage passe-partout blanc', N'correspond au prix de la photographie suite au choix du « Format » auquel on ajoute 40%.', 140, N'/assets/img/finish-5.PNG', 5)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (4, N'Support aluminium', N'Support aluminium', N'Tirage controllé sur support aluminium', N'correspond au prix de la photographie suite au choix du « Format » auquel on ajoute 160%.', 260, N'/assets/img/finish-1.PNG', 2)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (5, N'Support aluminium avec verre acrylique', N'Support aluminium avec verre acrylique', N'Tirage controllé sur support aluminium avec finition protectrice eb verre acrylique accentuant les contrastes et les coulours', N'correspond au prix de la photographie suite au choix du « Format » auquel on ajoute 235%.', 335, N'/assets/img/finish-2.PNG', 2)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (6, N'Tirage sur papier photo', N'Tirage sur papier photo', N'Tirage sur papier photo expédié roulé à accrocher ou à encadrer', N'correspond au prix de la photographie suite au choix du « Format ». Il n’y a pas de surcout induit par le choix de cette finition.', 100, N'/assets/img/finish-3.PNG', 2)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (7, N'Support aluminium', N'Support aluminium', N'Tirage controllé sur support aluminium', N'correspond au prix de la photographie suite au choix du « Format » auquel on ajoute 160%.', 260, N'/assets/img/finish-1.PNG', 3)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (8, N'Support aluminium avec verre acrylique', N'Support aluminium avec verre acrylique', N'Tirage controllé sur support aluminium avec finition protectrice eb verre acrylique accentuant les contrastes et les coulours', N'correspond au prix de la photographie suite au choix du « Format » auquel on ajoute 235%.', 335, N'/assets/img/finish-2.PNG', 3)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (9, N'Tirage sur papier photo', N'Tirage sur papier photo', N'Tirage sur papier photo expédié roulé à accrocher ou à encadrer', N'correspond au prix de la photographie suite au choix du « Format ». Il n’y a pas de surcout induit par le choix de cette finition.', 100, N'/assets/img/finish-3.PNG', 3)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (10, N'Support aluminium', N'Support aluminium', N'Tirage controllé sur support aluminium', N'correspond au prix de la photographie suite au choix du « Format » auquel on ajoute 160%.', 260, N'/assets/img/finish-1.PNG', 4)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (11, N'Support aluminium avec verre acrylique', N'Support aluminium avec verre acrylique', N'Tirage controllé sur support aluminium avec finition protectrice eb verre acrylique accentuant les contrastes et les coulours', N'correspond au prix de la photographie suite au choix du « Format » auquel on ajoute 235%.', 335, N'/assets/img/finish-2.PNG', 4)
INSERT [dbo].[Finishes] ([Id], [FrName], [EnName], [FrDescription], [EnDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (12, N'Tirage sur papier photo', N'Tirage sur papier photo', N'Tirage sur papier photo expédié roulé à accrocher ou à encadrer', N'correspond au prix de la photographie suite au choix du « Format ». Il n’y a pas de surcout induit par le choix de cette finition.', 100, N'/assets/img/finish-3.PNG', 4)
SET IDENTITY_INSERT [dbo].[Finishes] OFF
SET IDENTITY_INSERT [dbo].[Formats] ON 

INSERT [dbo].[Formats] ([Id], [FrName], [EnName], [Size], [PercentagePriceChange], [ImageUrl], [Description]) VALUES (2, N'Grand', N'Great', N'60x75', 260, N'/assets/img/size-big.PNG', N'Photographie montée sur aluminium, édition limitée 500 exemplaires')
INSERT [dbo].[Formats] ([Id], [FrName], [EnName], [Size], [PercentagePriceChange], [ImageUrl], [Description]) VALUES (3, N'Géant', N'Giant', N'100x125', 520, N'/assets/img/size-giant.PNG', N'Photographie montée sur aluminium, édition limitée 200 exemplaires')
INSERT [dbo].[Formats] ([Id], [FrName], [EnName], [Size], [PercentagePriceChange], [ImageUrl], [Description]) VALUES (4, N'Collector', N'Collector', N'120x150', 1300, N'/assets/img/size-collector.PNG', N'Photographie montée sur aluminium, édition limitée 100 exemplaires')
INSERT [dbo].[Formats] ([Id], [FrName], [EnName], [Size], [PercentagePriceChange], [ImageUrl], [Description]) VALUES (5, N'Classique', N'Classical', N'24x30', 130, N'/assets/img/size-classic.PNG', N'Tirage encadré, édition limitée à 5000 exemplaires da')
SET IDENTITY_INSERT [dbo].[Formats] OFF
SET IDENTITY_INSERT [dbo].[Frames] ON 

INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (1, N'Sans encadrement', N'Without supervision', N'Tirage contrecollé sur support aluminium', 100, N'/assets/img/frame-1.PNG', 2)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (2, N'Encadrement noir satin', N'Black satin frame', N'Caisse américaine noire satinée assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-2.PNG', 2)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (3, N'Encadrement blanc satin', N'Satin white frame', N'Caisse américaine blanche satinée assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-3.PNG', 2)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (4, N'Encadrement noyer', N'Walnut frame', N'Caisse américaine en noyer massif assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-4.PNG', 2)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (5, N'Encadrement chêne', N'Oak frame', N'Caisse américaine en chêne massif assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-5.PNG', 2)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (8, N'Aluminium noir', N'Black aluminum', N'Format 50x40cm', 100, N'/assets/img/frame-6.JPG', 5)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (9, N'Bois blanc', N'White wood', N'Format 50x40cm', 100, N'/assets/img/frame-7.JPG', 5)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (10, N'Acajou mat', N'Mahogany mat', N'Format 50x40cm', 100, N'/assets/img/frame-8.JPG', 5)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (11, N'Aluminium brossé', N'Brushed aluminum', N'Format 50x40cm', 100, N'/assets/img/frame-9.JPG', 5)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (12, N'Sans encadrement', N'Without supervision', N'Tirage contrecollé sur support aluminium', 100, N'/assets/img/frame-1.PNG', 3)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (13, N'Encadrement noir satin', N'Black satin frame', N'Caisse américaine noire satinée assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-2.PNG', 3)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (14, N'Encadrement blanc satin', N'Satin white frame', N'Caisse américaine blanche satinée assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-3.PNG', 3)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (15, N'Encadrement noyer', N'Walnut frame', N'Caisse américaine en noyer massif assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-4.PNG', 3)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (16, N'Encadrement chêne', N'Oak frame', N'Caisse américaine en chêne massif assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-5.PNG', 3)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (17, N'Sans encadrement', N'Without supervision', N'Tirage contrecollé sur support aluminium', 100, N'/assets/img/frame-1.PNG', 4)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (18, N'Encadrement noir satin', N'Black satin frame', N'Caisse américaine noire satinée assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-2.PNG', 4)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (19, N'Encadrement blanc satin', N'Satin white frame', N'Caisse américaine blanche satinée assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-3.PNG', 4)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (20, N'Encadrement noyer', N'Walnut frame', N'Caisse américaine en noyer massif assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-4.PNG', 4)
INSERT [dbo].[Frames] ([Id], [FrName], [EnName], [FrDescription], [PercentagePriceChange], [ImageUrl], [FormatId]) VALUES (21, N'Encadrement chêne', N'Oak frame', N'Caisse américaine en chêne massif assemblée à la main, apportant un effet de luminosité et de profondeur', 145, N'/assets/img/frame-5.PNG', 4)
SET IDENTITY_INSERT [dbo].[Frames] OFF
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (1, CAST(N'2019-10-29T12:06:11.0510776' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (2, CAST(N'2019-10-29T12:07:11.8197463' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (3, CAST(N'2019-10-29T12:12:23.9360671' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (4, CAST(N'2019-10-29T12:13:22.0723011' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (5, CAST(N'2019-10-29T12:15:33.2667479' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (6, CAST(N'2019-10-29T12:16:49.9571603' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (7, CAST(N'2019-10-29T12:16:50.8211056' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (8, CAST(N'2019-10-29T12:16:51.3803998' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (9, CAST(N'2019-10-29T12:16:52.9290877' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (10, CAST(N'2019-10-29T12:19:04.6252986' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (18, CAST(N'2019-10-29T12:57:01.3490591' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (19, CAST(N'2019-10-29T13:33:47.6046132' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (20, CAST(N'2019-10-29T13:35:18.6969471' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (21, CAST(N'2019-10-29T13:36:22.4126495' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (22, CAST(N'2019-10-29T13:37:49.7189758' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (23, CAST(N'2019-10-29T13:42:30.8946941' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (24, CAST(N'2019-10-29T13:43:42.0785966' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (25, CAST(N'2019-10-29T13:44:16.6375609' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (26, CAST(N'2019-10-29T13:48:20.3718009' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (27, CAST(N'2019-10-29T13:49:18.6733204' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (28, CAST(N'2019-10-29T13:55:24.2629343' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (33, CAST(N'2019-10-29T14:19:01.6718771' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (37, CAST(N'2019-10-29T14:26:39.3214534' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (38, CAST(N'2019-10-29T14:37:10.7014537' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (39, CAST(N'2019-10-29T14:44:17.0535645' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (40, CAST(N'2019-10-29T14:45:31.4801878' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (41, CAST(N'2019-10-29T15:54:43.7092096' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (42, CAST(N'2019-10-29T16:30:51.5980598' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (43, CAST(N'2019-10-29T16:31:09.2550157' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (44, CAST(N'2019-10-29T16:31:10.7144946' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (45, CAST(N'2019-10-29T16:36:10.9509970' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (46, CAST(N'2019-10-29T16:50:56.4593910' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (47, CAST(N'2019-10-29T16:54:03.5054881' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (48, CAST(N'2019-10-29T16:54:07.8566789' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (49, CAST(N'2019-10-29T16:54:08.6561583' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (50, CAST(N'2019-10-29T17:03:09.3615733' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Orders] ([Id], [OrderDate], [Delivery], [DeliveryCosts], [AddresStreet], [AddressNumber], [AddressPostalCode], [AddressCity], [AdressCountry], [GalleryId], [UserId], [Status]) VALUES (51, CAST(N'2019-10-29T17:06:47.7206865' AS DateTime2), NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL)
SET IDENTITY_INSERT [dbo].[Orders] OFF
SET IDENTITY_INSERT [dbo].[Themes] ON 

INSERT [dbo].[Themes] ([Id], [Name], [Description]) VALUES (1, N'Nature', N'La photo nature fait rêver d''ailleurs, laisse libre cours à l''imagination et nous laisse songeur. On s''imagine aux côtés de ces animaux exotiques, parcourant ces paysages enchanteurs, en quête d''aventures et de beauté. Panorama naturel, scène de vie animale, photo de fleurs ou photo de nature morte, la photo naturaliste est un art à part entière qui nous fait voyager.')
INSERT [dbo].[Themes] ([Id], [Name], [Description]) VALUES (2, N'Voyage', N'Partez à la rencontre de nouvelles cultures, découvrez les photos de Martinique, de Londres, d''Italie ou d''Inde, partez en safari photo en Afrique du Sud... La photographie capture les instants uniques et authentiques des voyages. Apportez une touche d''exotisme et d''aventure chez vous, et décorez votre intérieur avec les plus beaux paysages du monde.')
INSERT [dbo].[Themes] ([Id], [Name], [Description]) VALUES (3, N'Noir & Blanc', N'Une photo d’art en noir et blanc est d''une poésie remarquable. Indémodable, elle met en valeur ses sujets, souligne les contrastes et les lignes, et rehausse la lumière pour un rendu sublime.')
SET IDENTITY_INSERT [dbo].[Themes] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [Email], [Gender], [FirstName], [LastName], [DateOfBirth], [Password], [Profile], [PhoneNumber]) VALUES (1, N'laurick.baumann@gmail.com', N'Male', N'Laurick', N'Baumanno', CAST(N'1996-12-12T00:00:00.0000000' AS DateTime2), N'toto', N'admin', N'0635323615')
INSERT [dbo].[Users] ([Id], [Email], [Gender], [FirstName], [LastName], [DateOfBirth], [Password], [Profile], [PhoneNumber]) VALUES (23, N'm@gmail.com', N'Male', N'Quentin', N'Micelio', CAST(N'1996-09-12T00:00:00.0000000' AS DateTime2), N'toto', N'artiste', N'0115555855')
INSERT [dbo].[Users] ([Id], [Email], [Gender], [FirstName], [LastName], [DateOfBirth], [Password], [Profile], [PhoneNumber]) VALUES (24, N'mechehoudo@gmail.com', N'Male', N'Fayçal', N'MECHEHOUDO', CAST(N'1984-12-12T00:00:00.0000000' AS DateTime2), N'toto', N'artiste', N'0635323615')
INSERT [dbo].[Users] ([Id], [Email], [Gender], [FirstName], [LastName], [DateOfBirth], [Password], [Profile], [PhoneNumber]) VALUES (25, N'avodagbeio@gmail.com', N'Male', N'Godwin', N'AVODAGBEIO', CAST(N'1986-12-13T00:00:00.0000000' AS DateTime2), N'toto', N'user', N'0523525855')
INSERT [dbo].[Users] ([Id], [Email], [Gender], [FirstName], [LastName], [DateOfBirth], [Password], [Profile], [PhoneNumber]) VALUES (29, N'picasso@gmail.com', N'Male', N'Pablo', N'PICASSO', CAST(N'1881-10-25T00:00:00.0000000' AS DateTime2), N'toto', N'user', N'0802021302')
INSERT [dbo].[Users] ([Id], [Email], [Gender], [FirstName], [LastName], [DateOfBirth], [Password], [Profile], [PhoneNumber]) VALUES (31, N'', NULL, NULL, NULL, CAST(N'1996-12-12T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[Articles] ADD  DEFAULT ((0)) FOR [NbSales]
GO
ALTER TABLE [dbo].[Finishes] ADD  DEFAULT ((0)) FOR [FormatId]
GO
ALTER TABLE [dbo].[Frames] ADD  DEFAULT ((0)) FOR [FormatId]
GO
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Addresses_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Addresses_Users_UserId]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [FK_Articles_Artists_ArtisteId] FOREIGN KEY([ArtisteId])
REFERENCES [dbo].[Artists] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [FK_Articles_Artists_ArtisteId]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [FK_Articles_Galleries_GalleryId] FOREIGN KEY([GalleryId])
REFERENCES [dbo].[Galleries] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [FK_Articles_Galleries_GalleryId]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [FK_Articles_Themes_ThemeId] FOREIGN KEY([ThemeId])
REFERENCES [dbo].[Themes] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [FK_Articles_Themes_ThemeId]
GO
ALTER TABLE [dbo].[Artists]  WITH CHECK ADD  CONSTRAINT [FK_Artists_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Artists] CHECK CONSTRAINT [FK_Artists_Users_UserId]
GO
ALTER TABLE [dbo].[Galleries]  WITH CHECK ADD  CONSTRAINT [FK_Galleries_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Galleries] CHECK CONSTRAINT [FK_Galleries_Users_UserId]
GO
ALTER TABLE [dbo].[OrderedArticles]  WITH CHECK ADD  CONSTRAINT [FK_OrderedArticles_Articles_ArticleId] FOREIGN KEY([ArticleId])
REFERENCES [dbo].[Articles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderedArticles] CHECK CONSTRAINT [FK_OrderedArticles_Articles_ArticleId]
GO
ALTER TABLE [dbo].[OrderedArticles]  WITH CHECK ADD  CONSTRAINT [FK_OrderedArticles_Finishes_FinishId] FOREIGN KEY([FinishId])
REFERENCES [dbo].[Finishes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderedArticles] CHECK CONSTRAINT [FK_OrderedArticles_Finishes_FinishId]
GO
ALTER TABLE [dbo].[OrderedArticles]  WITH CHECK ADD  CONSTRAINT [FK_OrderedArticles_Formats_FormatId] FOREIGN KEY([FormatId])
REFERENCES [dbo].[Formats] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderedArticles] CHECK CONSTRAINT [FK_OrderedArticles_Formats_FormatId]
GO
ALTER TABLE [dbo].[OrderedArticles]  WITH CHECK ADD  CONSTRAINT [FK_OrderedArticles_Frames_FrameId] FOREIGN KEY([FrameId])
REFERENCES [dbo].[Frames] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderedArticles] CHECK CONSTRAINT [FK_OrderedArticles_Frames_FrameId]
GO
ALTER TABLE [dbo].[OrderedArticles]  WITH CHECK ADD  CONSTRAINT [FK_OrderedArticles_Orders_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderedArticles] CHECK CONSTRAINT [FK_OrderedArticles_Orders_OrderId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Galleries_GalleryId] FOREIGN KEY([GalleryId])
REFERENCES [dbo].[Galleries] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Galleries_GalleryId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Users_UserId]
GO
ALTER TABLE [dbo].[PaymentDetails]  WITH CHECK ADD  CONSTRAINT [FK_PaymentDetails_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[PaymentDetails] CHECK CONSTRAINT [FK_PaymentDetails_Users_UserId]
GO
